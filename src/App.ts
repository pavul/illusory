//this is the app created with electron, this will be loaded by main.js
//which is the entry point that electron uses when starts

import { app, BrowserWindow } from "electron";
import path = require("path")
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// const path = require('node/path')
// const path = require('path');
// const os = require('os');
// const fs = require('fs');

export class App
{

    width:number=800;
    height:number=600;

    window:any ;
    gameList:any=[];
    elApp:any;


    async start()
    {
        this.elApp = app;

           await app.on('ready', ()=>{
                 this.createWindow()
            });
    }

    async createWindow()
    {
        this.window= new BrowserWindow({
            width:this.width,
            height:this.height,
            webPreferences:{
                nodeIntegration:true,
            contextIsolation:true,
            // preload:path.join(__dirname+'preload.js')
            }
        });

        const indexHtml = path.join( __dirname+'/index.html');
        this.window.loadFile( indexHtml );
        this.window.setMenu(null);
        //to open devtools and console
        // this.window.webContents.openDevTools();

    }


}