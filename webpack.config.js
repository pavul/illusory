
//const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");
// const path = require('node:path')

module.exports = {
    // for files that should be compiled for electron main process
    target: 'electron-main',

    entry: './src/main.ts',
    devServer: {
        static: path.join(__dirname, "Illusory"),
        compress: true,
        port: 4040,
    },
    output: {
        filename: 'illusory.js',
        path: path.resolve(__dirname, 'Illusory')
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    configFile: path.resolve('./tsconfig.json'),
                }
            },

        ]
    },
    devtool: 'source-map',
    mode: "development",
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: path.resolve(__dirname, "src/index.html"), to: path.resolve(__dirname, "Illusory/") },
                { from: path.resolve(__dirname, "src/img/"), to: path.resolve(__dirname, "Illusory/img") }
            ],
        }),
    ]
    
};